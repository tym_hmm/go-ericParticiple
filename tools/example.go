package main

import (
	"flag"
	"fmt"
	"gitee.com/tym_hmm/go-ericParticiple"
	"os"
)

var (
	text = flag.String("text", "中国互联网历史上最大的一笔并购案", "要分词的文本")
)

func main() {
	flag.Parse()
	dFile:="./data/dictionary.txt"
	_,err:=os.Stat(dFile)
	if os.IsNotExist(err){
		fmt.Println("不存在")
		return
	}
	var ericPart ericParticiple.Segmenter
	ericPart.LoadDictionary("./data/dictionary.txt")

	segments := ericPart.Segment([]byte(*text))
	fmt.Println(ericParticiple.SegmentsToString(segments, true))
}
